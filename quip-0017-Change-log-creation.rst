QUIP: 17
Title: Change log creation
Author: Mitch Curtis
Status: Active
Type: Process
Created: 2020-08-25
Post-History: https://lists.qt-project.org/pipermail/development/2019-November/037927.html

Overview
========

The Qt framework uses change logs as a means of informing users about new
features, important behavior changes, and bug fixes. Before each release, a
change log file is generated from the section of git commit messages that start
with the [ChangeLog] keyword.

Content
=======

Do include:
- Noteworthy features
- Significant changes
- Changes that affect many users
- Changes that affect compatibility
- Changes to information about third-party code (see QUIP-4)

Do not include:

- Fixes in tests
- Documentation fixes
- Pure code cleanup or refactoring
- Fixes for regressions introduced in not-yet-released commits
- Anything that is not relevant to users

Format
======

- A [ChangeLog] entry may span multiple lines and ends with an empty line.
- You may add more than one [ChangeLog] entry, each in a separate paragraph,
  if there are several ways the change affects users.
- Each entry starts with a sequence of tags, each in square brackets, the
  first of which is [ChangeLog].

  - If the Git repository contains multiple modules, use the module name as a
    tag to indicate the area of the change, e.g. [QtCore].
  - Optionally specify a class or subtopic as an additional tag, e.g.:
    [QtNetwork][QSslSocket]
  - Other common tags are:

    - [General]
    - [Important Behavior Changes]
    - [Potentially Source-Incompatible Changes]
    - [Deprecation Notices]
    - [Platform Specific Changes]
    - [Windows]
    - [macOS]
    - [Linux/XCB]
    - [Third-Party Code]
- After the tags, describe how the change impacts users of the relevant
  component.

  - Try to integrate the ChangeLog entry into the surrounding commit message to
    avoid redundancy.
  - If the change resolves a security issue, summarise the issue and mention any
    relevant `CVE <https://cve.mitre.org>`_ or kindred identifiers.
  - The description should use either simple past ("Fixed …") or be explicit
    about referring to the current state ("… does now …").
  - Make sure the entry is reasonably self-contained. If you fail to formulate a
    meaningful description, it's probably not useful information to start with.
- In summary, the entry should look like this: ::

   [ChangeLog][module][class/topic] description of the really important change
   that was just made (on several lines).

Tools
=====

The createchangelog tool in the qtqa.git module is currently the most
feature-complete tool for change log creation. Documentation for it
can be found in its README under src/createchangelog.
