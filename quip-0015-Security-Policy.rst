QUIP: 15
Title: Qt Project Security Policy
Author: Volker Hilsheimer
Status: Active
Type: Process
Content-Type: text/x-rst
Created: 2019-05-21
Post-History: https://lists.qt-project.org/pipermail/development/2019-May/036030.html
              https://lists.qt-project.org/pipermail/development/2020-June/039672.html

Qt Project Security Policy
==========================

This QUIP documents the security policy of the Qt project. The goal is to inform
users of Qt about how the project handles security issues in Qt, and to document
processes that enable contributors to the Qt project to participate in the
prevention and handling of suspected vulnerabilities.

The Core Security Team
----------------------

The Core Security Team of Qt developers with `Approver`_ privileges is established
with the responsibility to ensure that this policy is followed. Any Approver may
volunteer to become a member of the Core Security Team, but needs to be supported
by at least one other approver. Members of the Core Security Team are added to
the security mailing list (security@qt-project.org).

.. _`Approver`: https://contribute.qt-project.org/quips/2#approvers

.. _`security mailing list`: https://lists.qt-project.org/listinfo/security


Proactive Measures to Prevent Security Issues
---------------------------------------------

The Qt project has implemented regular processes that help us reduce the risk of
introducing security vulnerabilities into the code-base or releasing them to users.

* The established `code review process`_ and `commit policy`_ prevent bad or
  compromised actors from committing malicious code or backdoors to the Qt
  code-base.
* The Qt source code is regularly scanned using static code analysis tools.
* Qt functionality that is designed to consume untrusted data is regularly tested
  using fuzzing.
* High priority issues discovered through static code scans and fuzz testing are
  reported as security issues, and addressed before the next release.
* For each Qt release, third-party components are updated to the latest version
  that is compatible with the respective Qt release.
* For each release, the Qt installer and other binary content in the released
  packages are scanned with antivirus tools.
* The Core Security Team monitors the CVE database for vulnerabilities in third-
  party components, and coordinates the application of necessary patches with
  the module maintainers.

.. _`code review process`: https://wiki.qt.io/Review_Policy

.. _`commit policy`: https://wiki.qt.io/Commit_Policy

Reporting Security Issues
-------------------------

Security issues should not be reported via the normal bugreports.qt.io tracker,
but should instead be sent to security@qt-project.org. For commercial licensees,
the issue can be reported to the Qt Company Support team via the support
portal, using the "Security Issues" category.

* The Core Security Team monitors and moderates incoming emails on business
  days (i.e. not including weekends), and approves all posts that are not spam.
* Approved posts are delivered to all recipients subscribed to the security
  mailing list.
* Any issue reported to security@qt-project.org should receive (at least) an
  acknowledgment of receipt within two business days.
* If there is no response in the above time frame (this should never happen),
  then the reporter should contact the Chief Maintainer directly.
* The Core Security Team controls membership of the security@qt-project.org;
  generally, all `Maintainers`_ are subscribed to this list.
* For security issues reported to the Qt Company Support team, they will be
  reported to security@qt-project.org and the reporter will be sent an
  acknowledgment that this has been done.

.. _`Maintainers`: https://contribute.qt-project.org/quips/2#maintainers

Handling of Reported Security Issues
------------------------------------

* The Core Security Team determines if an issue falls within the purview of an
  existing Maintainer; if so, then they ensure that the Maintainer is informed.
* Any issue reported should be triaged by the Maintainer to determine the risk
  it poses to end-users of Qt within four business days of the initial report
  to security@qt-project.org.
* Until the triaging is done and the analysis has been shared with the Core
  Security Team via email to security@qt-project.org, any reported issue is
  assumed to have P0 priority.
* Any issue determined to be high risk should be immediately reported to the
  Chief Maintainer by the security team.
* If no triaging analysis is shared with the Core Security Team within seven
  days, then the Core Security Team should escalate the issue to the Chief
  Maintainer.
* Maintainers are responsible for addressing any security issues in the code
  they maintain.
* The Core Security Team is responsible for ensuring that the issue is addressed
  according to this policy, supports the Maintainer, and coordinates with other
  relevant contributors and third parties.
* If the reported vulnerability is in third-party code, then the Core Security
  Team coordinates with Maintainers and the respective third party.
* If the reported vulnerability is in commercially licensed only code, then the
  Qt Company will handle it accordingly.
* Reported issues that are assessed to not have an impact on security can be
  handled as regular bug reports, and may be filed by a suitable party in the
  normal bugreports.qt.io tracker.

How will Issues be Disclosed?
-----------------------------

* The Core Security Team ensures that confirmed security issues in Qt code
  are listed in the `Common Vulnerabilities and Exposures database`_, and
  if needed files them after the risk assessment.
* Security issues will be disclosed by an email to the announce@qt-project.org
  mailing list and to all commercial licencees once the CVE entry is published.
* All members of the Core Security Team must have posting rights for the
  announce@qt-project.org list for this purpose.
* All security announcements will be made on behalf of the Qt Project, though
  credit to those responsible for identifying and addressing the issue should
  be made.
* The security announcement should describe:
   * The security issue.
   * How and when it will be addressed.
   * Sufficient technical detail to allow users of Qt to determine the impact
     on their applications.
   * How to fix or work around the issue in existing installations and
     applications.
* If an issue requires clarification beyond the security announcement, then this
  can be done using the development mailing list or the interest mailing list.
  This is not expected to be required for all security announcements and does
  not replace the formal notification via the announce mailing list.
* Where possible, early notification should be sent to packagers such as
  distribution contacts. These notifications should be considered privileged
  information. A security-announce list for distribution contacts will be used
  for this purpose.
* Membership of the security-announce mailing list should be kept small, and
  granted only by agreement with the Core Security Team. This membership can
  be revoked at any time, with no explanation required.
* Where possible, packagers should be informed directly of which SHA1s they
  should cherry-pick in order to get a security fix.
* For every minor version of Qt, fixed security issues will be listed in the
  change file of the first patch release which contains the fix.

.. _`Common Vulnerabilities and Exposures database`: https://cve.mitre.org

What Versions of Qt are Covered by this Policy?
-----------------------------------------------

While we are interested in reports against any Qt version that is still
maintained, fixes are only guaranteed to be provided for:

* The latest released version.
* The preceding minor version.

Fixes for earlier versions, in particular LTS releases, may be provided, but the
Qt project makes no commitment to do so. Other groups such as The Qt Company
may choose to make such fixes available, but that is outside the scope of the
Qt project.
