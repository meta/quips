QUIP: 18
Title: License specification in Qt's modules
Author: Lucie Gerard,
        Jörg Bornemann
Status: Active
Type: Informational
Created: 2022-09-26
Post-History:
  https://lists.qt-project.org/pipermail/development/2023-February/043648.html
  https://lists.qt-project.org/pipermail/development/2024-February/045028.html
  https://lists.qt-project.org/pipermail/development/2025-February/046121.html

Overview
========

This QUIP specifies the licenses that are used in different parts of a
typical Qt module repository and also provides helpful guidance about
how to express which license source code is under.

License header format
=====================

The license header follows the SPDX standard [0]_.

There is at least one copyright line and exactly one license
expression line::

  Copyright (C) 2023 The Qt Company Ltd.
  SPDX-License-Identifier: <spdx-license-expression>

A copyright line for code written by a contributor outside the Qt
Company can include that contributor's name (or a legal entity they
represent) in the Copyright header.

The copyright line should include the year the file was created, or -
in the case of additional copyright lines - the year the additional
copyright notice was added. Updating the copyright year when changes
are made to the file is possible.

The copyright line may be prefixed with `SPDX-FileCopyrightText:`.

The `<spdx-license-expression>` must be a valid expression, according
to the SPDX standard as defined in [1]_.

The license identifiers used in a license expression are either from
the list of known licenses [2]_ or prefixed with `LicenseRef-`.

The license files are placed into a `LICENSES` directory below the Qt
module repository's root. The license files are named after the SPDX
license identifier followed by ``.txt`` [3]_.

Classification of files
=======================

There are various kinds of files in a Qt module repository that should
include license headers. Here, we classify these files in order to
identify which files should be under which licenses.

Note that files that cannot use inline headers need to advertise their
copyright and  license using an adjacent REUSE.toml file [4]_.

The documentation landing page of a Qt module contains a "Licenses and
Attributions" section that defines the licenses under which the module
and plugin implementation is available. See the list of Qt modules
[7]_.

Module and plugin implementation
--------------------------------

Files that belong to the implementation of Qt modules and plugins. The
usual locations for module source code are the subdirectories ``src/``
and ``plugins/``.

There are two licensing models for module and plugin implementations:
GPL-only and one that is additionally licensed under LPGL.

The license expression permitting the LPGL is
``LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR
GPL-3.0-only``.

The GPL-only license expression is ``LicenseRef-Qt-Commercial OR
GPL-3.0-only``.

The license expression for files that are statically linked with user
code - even for dynamic builds of Qt - is
``LicenseRef-Qt-Commercial OR BSD-3-Clause``.

Examples and Snippets
---------------------

Files that belong to the implementation of Qt examples. The usual
locations for examples is the ``examples/`` subdirectory.

Also, any source code that appears in the documentation. The usual locations are
``snippets/`` directories throughout the Qt module repository's directory
tree.

The license expression for examples and snippets is
``LicenseRef-Qt-Commercial OR BSD-3-Clause``.

Tests
-----

Files that belong to the implementation of Qt tests. The usual
location is the ``tests/`` subdirectory.

The license expression for tests is ``LicenseRef-Qt-Commercial OR
GPL-3.0-only``.

Qt Translations
---------------

The usual location and file pattern for Qt translation files is ``translations/*.ts``.

The license expression for Qt translation files is ``LicenseRef-Qt-Commercial OR
BSD-3-Clause``.

If the translation file is within a ``tests/`` subdirectory it is
licensed as a test file. We also have TS files in some examples.
Those would be licensed as examples.

Tools and Utils
---------------

Files that belong to the implementation of Qt tools that are for use
by Qt users. The usual location is the ``src/tools/`` subdirectory.

Also considered tools are script and shell files,
as long as they are not part of an example or a documentation snippet.
Such files are usually using the following file name patterns:

- ``*.sh``
- ``*.py``
- ``*.pl``
- ``*.bat``
- ``*.ps1``

Finally, files that are located in a /util/ directory.

The license expression for tools and utils is ``LicenseRef-Qt-Commercial OR
GPL-3.0-only WITH Qt-GPL-exception-1.0``.

Documentation
-------------

All files that are needed to generate the documentation:

- ``*.qdoc``
- ``*.qdoc.sample``
- ``*.qdocinc``
- ``*.qdocconf``
- documentation assets, like images, typically located in ``doc/images`` folders,
  including screencasts or animations.

Following informative files are also considered documentation:

- ``README``
- ``qt_attribution.json`` (also used to generate documentation)

The license expression for documentation is ``LicenseRef-Qt-Commercial
OR GFDL-1.3-no-invariants-only``.

Build System Files
------------------

All files that contribute to Qt's build system.
That includes the following file name patterns:

- ``CMakeLists.txt``
- ``*.cmake``
- ``*.cmake.conf``
- ``*.pro``
- ``*.pri``
- ``*.prf``
- ``configure``
- ``ci_config_linux.json``
- ``*.cfg``
- ``*.qrc``
- ``*.plist``
- ``*.tag``
- ``*.yaml``

and files within the ``coin`` folder (unless their suffix have them fall into
an other category, like tools for example).

If such a file is present in a snippet directory, it is licensed as
a documentation snippets file.
If such a file is present in an example directory, it is licensed as
an example file.

The license expression for build system files is ``BSD-3-Clause``.

Infrastructure
--------------

Files that define development environment configurations and coding standards.
These files typically include version control settings, code formatting rules,
editor configurations, and license setting and definition.
Common file patterns include:

- ``.gitattributes``
- ``.gitignore``
- ``.gitmodules``
- ``.gireview``
- ``_clang-format``
- ``licenseRule.json``
- ``REUSE.toml``

The license expression for infrastructure files is
``LicenseRef-Qt-Commercial OR BSD-3-Clause``.

Testing License Usage
=====================

The Qt CI can enforce correct usages of license files.
This requires a 'licenseRule.json' file to exist in the top-level
directory of the Qt module's repository. This file contain
declarative rules, based on file suffix and directory,
and should follow the rules set out in this QUIP.
They do allow for exceptions and special cases though, if necessary.

Related Work
============

The "Software License Agreement between The KDE Free Qt Foundation and
The Qt Company Oy" [5]_ poses restrictions for the licenses of the Qt
Free Edition modules.

To a certain extent, Qt follows REUSE [6]_. REUSE is a guideline on
how to specify the licenses in a project. We aim at full
compliance.

The reuse tool is also used to generate a “software bill of materials”
(SBOM) document for the Qt source code. Using SPDX format, such a source SBOM
lists copyright and licensing information for all files within a module.
In the CI, source SBOM is generated from the SDPX-formatted copyright
and license information in the Qt sources and in the REUSE.toml files.
The source SBOM is then checked against 'licenseRule.json'.

References
==========

.. [0] https://spdx.github.io/spdx-spec/v2.3/
.. [1] https://spdx.github.io/spdx-spec/v2.3/SPDX-license-expressions/
.. [2] https://spdx.org/licenses/
.. [3] https://reuse.software/tutorial/#step-1
.. [4] https://reuse.software/spec-3.2/#reusetoml
.. [5] https://kde.org/community/whatiskde/Software_License_Agreement_2015_Text.pdf
.. [6] https://reuse.software/spec/
.. [7] https://doc.qt.io/qt-6/qtmodules.html
