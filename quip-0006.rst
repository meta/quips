QUIP: 6
Title: Acceptable Source-Incompatible Changes
Author: Marc Mutz
Status: Active
Type: Implementation
Post-History: https://lists.qt-project.org/pipermail/development/2016-July/026527.html
              https://lists.qt-project.org/pipermail/development/2017-January/028340.html
              https://lists.qt-project.org/pipermail/development/2022-December/043428.html
Created: 2017-01-13

========================================
 Acceptable Source-Incompatible Changes
========================================

Motivation
----------

Qt strives to keep source compatibility as much as possible within a major
Qt release, i.e. between minor releases.

We acknowledge that there can be exceptional situations where adding new
functionality or fixing issues requires changes that might break existing
code.

This QUIP offers guidelines on which types of source-incompatible
changes may be acceptable between minor releases.

It originated from `the author's post
<https://lists.qt-project.org/pipermail/development/2016-July/026543.html>`_
in the `Source break policy for function overloads
<https://lists.qt-project.org/pipermail/development/2016-July/026527.html>`_
mailing-list thread.


Classification of Source-Incompatible Changes
---------------------------------------------

We classify source-incompatible changes (SiCs) into two categories: A and B.

Category A SiCs break existing code, but can be worked around in user
code without introducing Qt version checks.

Category B SiCs break existing code, and need to be worked around in
user code using Qt version checks, or similar techniques, such as
`SFINAE <http://en.cppreference.com/w/cpp/language/sfinae>`_.

Category A SiCs are acceptable, while Category B SiCs are not.

Category B SiCs can be made acceptable when hidden behind an *opt-in* macro.
Deprecations and QT_NO_x macros are the primary examples of such opt-in
mechanisms.

User Notification
-----------------

Accepted SiCs need be communicated to users by way of changelog entries at the
library level, using the tag [Potentially Source-Incompatible Changes], see
`QUIP 17 <quip-0017-Change-log-creation.html>`_ for details.

Examples
--------

This list is not exhaustive. Issues not listed here should be
discussed on the mailing-list and then added here.

+-------+-------------------------------------------------------------+-------+--------+
|       |                                                             | Classification |
|   #   | Source-incompatible Change                                  +-------+--------+
|       |                                                             | Cat A | Cat B  |
+-------+-------------------------------------------------------------+-------+--------+
|  A.1  | Adding an overload of a function                            |   X   |        |
+-------+-+-----------------------------------------------------------+-------+--------+
|  B.1  | | Exception: when causing ambiguities                       |       |   X    |
+-------+-+-----------------------------------------------------------+-------+--------+
|  A.2  | Adding Q_OBJECT to a QObject subclass that lacks it.        |   X   |        |
+-------+-------------------------------------------------------------+-------+--------+
|  A.3  | Deprecating a function/class/typedef (even though it breaks |   X   |        |
|       |     -Werror=deprecated builds)                              |       |        |
+-------+-------------------------------------------------------------+-------+--------+
|  A.4  | Removing an include from a public header file               |   X   |        |
+-------+-------------------------------------------------------------+-------+--------+
|  A.5  | Removing top-level const from return types (see footnote 1) |   X   |        |
+-------+-------------------------------------------------------------+-------+--------+
|  B.6  | Removing/restricting public API (even if binary-compatible) |       |   X    |
+-------+-+-----------------------------------------------------------+-------+--------+
|  A.6  | | Exception: when preventing API misuses at compile-time,   |   X   |        |
|       | |            e.g. when constraining templates (further).    |       |        |
+-------+-+-----------------------------------------------------------+-------+--------+
|  B.7  | Moving declarations between headers that don't include each |       |   X    |
|       |   other (see footnote 2).                                   |       |        |
+-------+-+-----------------------------------------------------------+-------+--------+
|  A.7  | | Exception: when moving classes                            |   X   |        |
|       | |           (we promise SC only for use of <QStyleHeaders>, |       |        |
|       | |            not the qstyleheaders.h)                       |       |        |
+-------+-+-----------------------------------------------------------+-------+--------+

¹ On compilers, such as MSVC, that mangle the return type, this is a BiC change
  if the function is exported. In this case, the const needs to be kept for these
  compilers until the next BC break, usually the next major Qt release.

² This is technically SiC A, if the target header exists in the X.Y.0 release of Qt in
  which the definition was introduced, but the project decided that we do not want these,
  because it would mean unnecessary churn for Qt users.

References
----------

- `Binary Compatibility Issues With C++
  <https://community.kde.org/Policies/Binary_Compatibility_Issues_With_C%2B%2B>`_

- `SFINAE <http://en.cppreference.com/w/cpp/language/sfinae>`_

- `Source break policy for function overloads
  <https://lists.qt-project.org/pipermail/development/2016-July/026527.html>`_
  mailing-list thread.
