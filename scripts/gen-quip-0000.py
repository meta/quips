#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2020 The Qt Company Ltd.
# Contact: http://www.qt.io/licensing/
#
# You may use this file under the terms of the CC0 license.
# See the file LICENSE.CC0 from this package for details.

import os.path
from email.parser import HeaderParser
from textwrap import dedent


class RootQuip (dict):
    def __init__(self, files):
        parser = HeaderParser()
        for quip in files:
            with open(quip, 'r', encoding='utf-8') as quipfp:
                self[quip] = parser.parse(quipfp)

    def last_name(names):  # tool function for owners; not a method
        while names and names[-1].startswith('<') and names[-1].endswith('>'):
            names.pop()
        return names[-1]

    @staticmethod
    def owners(authors, final=last_name):
        return ', '.join(final(s) for s in (a.split() for a in authors.split(',')))
    del last_name

    def quip_entries(self):
        fmt = ' <tr><td>%4s</td><td><a href="%s.html">%04d</a></td><td>%s</td><td>%s</td></tr>'
        for filename in sorted(self):
            q = self[filename]
            num = int(q['QUIP'])
            name = os.path.splitext(os.path.basename(filename))[0]
            yield fmt % (q['Type'][:4], name, num, q['Title'], self.owners(q['Author']))

    preamble = dedent("""\
        QUIP: 0
        Title: QUIP Index
        Author: Louai Al-Khanji, Edward Welbourne
        Status: Active
        Type: Informational
        Created: 2016-09-16
        Post-History: 2017-03-15

        QUIP Index
        ==========

        This QUIP contains the index of all finalized Qt User-Submitted
        Improvement Proposals, known as QUIPs.  A list of reviews of quip
        module changes, including any QUIPs under development, can be found in
        the code-review listing [0]_.

        The following QUIPs exist:

        .. raw:: html

         <table>
         <tr><th>type</th><th>number</th><th>title</th><th>owners</th></tr>
        """)

    endpiece = dedent("""
         </table>

        These documents are generated from ReStructuredText sources which can
        be found at [1]_. Proposals for change or new QUIPs can be
        contributed, via the Qt Project's usual contribution process [2]_, to
        the repository [3]_.  See QUIPs 1 through 3, above, for further
        details.

        .. [0] https://codereview.qt-project.org/q/project:meta/quips

        .. [1] https://code.qt.io/cgit/meta/quips.git/

        .. [2] http://wiki.qt.io/Qt_Contribution_Guidelines

        .. [3] https://codereview.qt-project.org/admin/repos/meta/quips
        """)

    def compose_body(self):
        return self.preamble + '\n'.join(self.quip_entries()) + self.endpiece


if __name__ == '__main__':
    import sys
    sys.stdout.reconfigure(encoding='utf-8')
    print(RootQuip(sys.argv[1:]).compose_body())
