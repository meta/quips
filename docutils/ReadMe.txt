== Materials specific to python's docutils ==

The present content of this directory is just the style-sheet,
quip.css, used for QUIPs; it is a copy of writers/pep_html/pep.css
such as may be found under /usr/share/docutils/ in a Debian
distribution.  We may be better off eliminating this directory
entirely by referencing docutils' existing copy of that pep.css,
instead of this quip.css copy of it.

The docutils.conf isn't presently used and I'm not even sure we *want*
to use it - if we *could* make the template not embed quip.css, we'd
need to arrange for quip.css to be in a suitable place in the
generated source tree.  It may well be easier to simply inline it ...
