QUIP: 10
Title: Reviewing API changes in preparation for release
Author: Edward Welbourne
Status: Active
Type: Process
Requires: QUIP-11
Created: 2018-04-17
Post-History: https://lists.qt-project.org/pipermail/development/2018-March/032338.html

Reviewing API changes in preparation for release
================================================

The Qt project makes various commitments about compatibility between versions.
In support of ensuring that we live up to those commitments,
and to check whether additions conform to our `API design principles`_,
we review changes to API as part of the preparation for a release.

Such review begins after feature freeze.
The new release's API is not finalized until the review is complete.
Any changes to API that happen after feature freeze must be reported
on the API review for the affected module.

Form of the review
------------------

The review takes the form of a code review, comparing the API-defining
files of each module at the last release with their counterparts for
the upcoming release.
A commit representing this change, for each module, can be generated
by scripts in the qtqa_ module's ``scripts/api-review/`` subdirectory,
named ``all-api-modules`` and ``api-review-gen``, which drive other scripts.
See ``all-api-modules --help`` for details.

Although the review is actually of a commit based on the earlier
release, it is reviewed as part of the work on the branch that is
being prepared for release.
Unlike other code reviews, it is thus not suitable for integration.

Organization of the review
--------------------------

When the review is to be started,
a task is opened in the Qt project's bug-tracking system and
treated as a blocker for the upcoming release.
Its issue identifier shall be used in the Task-number: footer of each
API review (see the ``--task-number`` option to ``api-review-gen``).

Once the API reviews are published, they are announced on the Qt developer
mailing list.
Contributors to the project and users of the Qt framework may then comment
on these reviews in the usual ways.

Any issues raised in review should be recorded in the bug-tracking system
as blockers of the primary review task and of the release.
The blocker's identifier shall be used as the Task-number: footer in any
commits that seek to resolve the issue.
Such issues are closed once the API correction has been merged and the
API review has been updated to include the fix.

Once each module's API review has been accepted by the module's
maintainers, the review is abandoned to indicate that it is complete.
No further changes to that module's API may be made for this release.

When all modules' API reviews have closed, the review task is resolved as Done.

References
----------

.. _qtqa: https://code.qt.io/cgit/qt/qtqa.git
.. _`API design principles`: https://wiki.qt.io/API_Design_Principles
