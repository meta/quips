QUIP: 7
Title: qt_attribution.json File Format
Author: Kai Köhne
Status: Active
Type: Implementation
Created: 2017-02-15
Post-History:
  https://lists.qt-project.org/pipermail/development/2017-March/029198.html
  https://lists.qt-project.org/pipermail/development/2023-February/043639.html
  https://lists.qt-project.org/pipermail/development/2023-February/043667.html
  https://lists.qt-project.org/pipermail/development/2023-September/044431.html
  https://lists.qt-project.org/pipermail/development/2024-March/045032.html

Overview
========

Qt modules document their 3rd party code in qt_attribution.json
files. The format is JSON-based and can be processed with the
qtattributionscanner tool.

This QUIP is about the file format of the qt_attribution.json files.
See QUIP 4 for the process on how to add and update 3rd party code to Qt.

File Format
===========

A qt_attribution.json file needs to contain one JSON object or an array of such
objects. Each object's properties have the following meanings:

Id
  Unique name for component, without spaces. All lower case. Mandatory.
Name
  Descriptive name of the component, without version number. Camel case.
  Mandatory.
QDocModule
  The qch file the documentation of the component should be part of. Mandatory.
QtParts
  Array with possible entries "examples", "tests", "tools", or "libs".
  Components that will be included in Qt libraries or plugins should set
  "libs", components that will be included in executables like Qt Designer or
  are part of the build infrastructure "tools".
  Optional, default is [ "libs" ].
QtUsage
  Free text on where the Third-Party Component in Qt is used, when it is
  included  (e.g. if it's limited to a specific platform), how to avoid it
  (e.g. by using configure options), and what functionality is missing in
  this case.
  Mandatory.
SecurityCritical
  Boolean attribute indicating whether the 3rd party code is security critical
  - that is, if it is part of code paths that process untrusted code, or is
  otherwise critical for security purposes. Optional, default is "false".
Origin
  Set to either "Provisioned" or "InSource" (the default). "Provisioned"
  indicates that the component is not part of the Qt sources, but provisioned
  in the CI. Supported in qtattributionsscanner 1.4 and onwards. Optional.
Path
  (Relative) path to the source directory. Default is same directory as the
  file. Should only be specified if Origin is set to "InSource". Optional.
Files
  Array of paths to the source files, relative to Path.
  Should only be specified if Origin is set to "InSource". Optional.
Description
  A short description of what the component is and is used for. Optional.
Homepage
  Homepage of the upstream project. Optional.
Version
  Version used from the upstream project. This can also be a revision number
  or commit hash from a version control system.
  Optional.
DownloadLocation
  Link to exact upstream version. Optional, unless SecurityCritical is
  set to "true".
Comment
  An arbitrary JSON object, for use by those maintaining third-party
  components, to leave notes to those doing future updates. JSON
  explicitly allows a name to be used repeatedly so separate comments
  can be placed alongside any fields to which they may be relevant.
License
  The license under which the component is distributed, preferably with the
  names from SPDX [1]_ 'Full Name'. Mandatory.
LicenseId
  SPDX License Identifier [1]_, or an SPDX Expression [2]_. Optional.
LicenseFile
  Relative path to file containing the license text. Optional for code in the
  Public Domain. Required for code outside of the public domain if neither
  LicenseFiles nor LicenseId are set.
LicenseFiles
  Array of relative file paths containing the license texts. Optional for code
  in the Public Domain. Required for code outside of the public domain if
  neither LicenseFile nor LicenseId are set.
Copyright
  Copyright lines. String or (since Qt 6.5) string array are accepted. Mandatory.

  The list can be edited for brevity by, for example, combining different
  copyright years for the same author. Terms like "All rights reserved" can be
  removed, too.

  If there are too many copyright lines in the sources, aggregating them into::

    Copyright YYYY The x project contributors

  or::

    Copyright YYYY <main author> and contributors

  is also acceptable.
CPE
  A CPE (Common Platform Enumeration) string for the component, formatted
  according to the CPE 2.3 specification [7]_.
  The format is as follows::

    cpe:<cpe_version>:<part>:<vendor>:<product>:<version>:<update>:<edition>:<language>:<sw_edition>:<target_sw>:<target_hw>:<other>

  Sample values can be found in the official CPE dictionary [8]_.
  An example for zlib would be::

    cpe:2.3:a:zlib:zlib:1.3:1:*:*:*:*:*:*

  The value should ideally include the version of the component in
  addition to the vendor and product names.
  The value can be a single string or an array of strings. Optional.
PURL
  A Package URL (PURL) for the component, formatted according to the
  PURL specification [9]_.
  The format is as follows::

    scheme:type/namespace/name@version?qualifiers#subpath

  There is no single database for PURLs, but a non-exhaustive database
  can be found at [10]_.
  Otherwise if the component is hosted on GitHub, the PURL is easily
  constructed based on the GitHub URL.
  An example for zlib would be::

    pkg:github/madler/zlib@v1.3.1

  The value should ideally include the version of the component, in
  addition to the namespace and name.
  Can be either a single string or an array of strings. Optional.

Specification of License Files
------------------------------

There are two ways of specifying license files for 3rd-party code.

Using SPDX License Identifiers
``````````````````````````````

For licenses that can be expressed with a valid SPDX license expression [1]_,
the corresponding license texts can be placed in
``LICENSES/<SPDX_IDENTIFIER>.txt`` files. The *LicenseId* property is required
in this case.

*LicenseFiles* and *LicenseFile* are not required.

A ``LICENSES/<SPDX_IDENTIFIER>.txt`` file can be created using the reuse
tool [6]_ by running::

  pip install reuse
  reuse download <SPDX_IDENTIFIER>

Directly specifying the license file path
`````````````````````````````````````````

With *LicenseFiles* or *LicenseFile* one can specify a list or just one license
file. The *LicenseId* property is optional in this case.

Related Work
============

The issue of attributing Third-Party Components correctly is known in a lot of
projects and companies. Some companies require elaborate documents about
Third-Party Components from their suppliers. Multiple projects have invented
their own formats for documenting Third-Party Components and licenses.

SPDX
----

SPDX [3]_ (Software Package Data Exchange) is a standard format for
communicating the components, licenses and copyrights associated with a
software package. It supports several file formats (XML, RDF, Key/Value), and
is aimed to be generated by code-scanning tools.

We have adapted the SPDX license list [1]_, but considered the standard itself
to be too heavy for our use. However, it should be possible to generate
(parts of) SPDX files out of the qt_attribution.json files.

debian/copyright files
----------------------

Debian has established a convention for making their debian/copyright files
machine-readable [4]_. This is meant to be used on the packaging level.

README.chromium files
---------------------

README.chromium files [5]_ are used by Google Chrome to generate the
'about://credits' page. Qt WebEngine uses them also to document the Third-Party
Components inside Chromium. Compared to qt_attribution.json files, the format
is not strictly enforced.

References
==========

.. [1] https://spdx.org/licenses/
.. [2] https://spdx.org/spdx-specification-21-web-version#h.jxpfx0ykyb60
.. [3] https://spdx.org/specifications
.. [4] https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
.. [5] https://src.chromium.org/viewvc/chrome/trunk/src/third_party/README.chromium.template
.. [6] https://github.com/fsfe/reuse-tool
.. [7] https://cpe.mitre.org/specification/
.. [8] https://nvd.nist.gov/products/cpe
.. [9] https://github.com/package-url/purl-spec
.. [10] https://github.com/scanoss/purl2cpe

