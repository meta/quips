QUIP:
Title:
Author: Comma-Separated List, With Continuation Lines
Status: { Active | Superseded | Withdrawn }
Type: { Implementation | Informational | Process }
[Qt-Version:]
[Content-Type: text/x-rst]
[Requires: QUIP-1]
Created: YYYY-MM-dd
Post-History: https://lists.qt-project.org/pipermail/development/YYYY-Month/nnnnnn.html
[Replaces:]
[Superseded-By:]

Title
=====

Introductory paragraph, motivation.
Use reStructuredText_ mark-up.
Run it through the checker_ to see how it'll look.

Various sections
----------------

Develop the theme and state the proposal.

References
----------

.. [0] QUIP-1 defines the form this template anticipates:
   https://code.qt.io/cgit/meta/quips.git/tree/quip-0001.rst

.. _reStructuredText: http://docutils.sourceforge.net/rst.html

.. _checker: http://rst.ninjs.org/
