QUIP: 21
Title: Using images in Qt documentation
Author: Leena Miettinen
Status: Active
Type: Process
Created: 2023-11-13
Post-History: https://lists.qt-project.org/pipermail/development/2023-November/044706.html

Overview
========

You can use the following types of images in Qt documentation:

- Screenshots
- Line diagrams
- Icons
- Animated images
- Embedded YouTube videos

Saving images
=============

QDoc can create references to images of any format. The image is shown if
the user's tool or browser supports the image format. Most browsers support the
following image formats:

- GIF (``.gif``)
- JPEG (``.jpg``)
- WebP (``.webp``)
- PNG (``.png``)
- SVG (``.svg``)

You can find reasons for picking a particular image format in the following
sections.

Locations
---------

Save images in the Git repositories in an ``images`` folder. QDoc looks for
images in folders that you specify as values of the ``imagedirs`` variable [0]_
in the documentation configuration file (``.qdocconf``).

You can create subfolders for particular types of images, such as icons or
images used in examples.

Considerations
--------------

Binary images can easily add megabytes to the Git history. To keep the history as
small as possible, the Git post-commit hooks remind you to try to keep image file
size below 50 KiB. To achieve this goal, crop images so that only relevant
information is visible.

When possible, save images in WebP format for a smaller image file size.

If you need to use the PNG format, optimize the images by using an image
optimization tool, such as ``optipng``. Optimization should not visibly
reduce image quality in the built documentation. If it does, save the
image as WebP or JPEG instead.

QDoc commands and macros
========================

Use QDoc commands to refer to images from the text:

- ``\image`` places the image on a separate line [1]_
- ``\inlineimage`` places the image inline, surrounded by text [2]_
- ``\youtube`` embeds a video from YouTube [3]_

Screenshots
===========

Qt tools, such as Qt Creator and Qt Design Studio, have the native look and feel
on Windows, Linux, and macOS. Therefore, screenshots can end up looking very
different depending on who takes them and which operating system they use.
Try to take the screenshots on one operating system, when possible.

Follow these guidelines when taking screenshots:

- Use the screen resolution of 1920x1080.
  (Available on the most commonly used screens, as of this writing.)
- Use display scaling 100%.
- Use your favorite tool to take the screenshot.
- Include only the part of the screen that you need.
  (You can crop the image also in the screen capture tool).
- Close all unnecessary views to avoid clutter.
- Do not scale or resize the images in the tool because it causes
  reduced visual quality and increases the file size. Also, the CSS we use
  online scales down images that are wider than 800 pixels.
- To highlight parts of the screenshot, use the images of numbers
  that are stored in [4]_.
- If you use PNG images, optimize them before you submit them to the repository.

Note: Do not rely on screenshots to present reasonable example values to
users, but always place example values also in the text.

Note: The selected screen resolution is a compromise that offers acceptable
quality with acceptable image file size. We are looking for solutions to using
high-DPI images [5]_.

Highlighting parts of the screen
---------------------------------

You can use number icons in screenshots to highlight parts of the screenshot
(instead of using arrows or borders, for example). You can then refer to the
numbers in text. For example, see [6]_.

This improves the consistency of the look and feel of Qt documentation,
and eliminates the need to describe parts of the UI in the text because
you can just insert the number of the element you are referring to in
brackets.

You can find images of numbers from 1 to 10 in [4]_.

To use the numbers, copy-paste the number images on the screenshot to the
places that you want to refer to from text.

Icons
=====

You can view Qt Documentation online [7]_ in dark and light modes. To make the
icons used in the docs visible in both modes, save icon files as gray-scale
images with a transparent background.

If you receive a large number of icons that are not visible in either light
or dark mode or have a solid background, run the ``recolordocsicons.py``
Python script from the ``qttools/util/recolordocsicons`` folder. For options
and examples, see [8]_.

Animated images
===============

Sometimes, it is easier to explain how something works by recording a short
animation in lossless WebP or GIF format. You can use any tool you like, such
as the screen recorder in Qt Creator [9]_ or ScreenToGif [10]_. Animated images
are typically bigger than screenshots, so try to make them as short and to the
point as you can.

Use the ``\image`` command [1]_ to refer to WebP and GIF files from the
documentation.

Note: If the animation is very long, consider recording a video and embedding
it from YouTube.

YouTube videos
==============

You can use the ``\youtube`` macro [3]_ to embed a YouTube video in the HTML.
When QDoc generates offline documentation (``.qch``), it adds an external
link to the video with a thumbnail image.

References
==========

.. [0] https://doc.qt.io/qt-6/22-qdoc-configuration-generalvariables.html#imagedirs
.. [1] https://doc.qt.io/qt-6/09-qdoc-commands-includingimages.html#image-command
.. [2] https://doc.qt.io/qt-6/09-qdoc-commands-includingimages.html#inlineimage-command
.. [3] ``macro.youtube`` in ``qtbase/doc/global/macros.qdocconf``
.. [4] ``qtdoc/doc/images/numbers/``
.. [5] https://bugreports.qt.io/browse/QTBUG-63366
.. [6] https://doc.qt.io/qtcreator/creator-quick-tour.html
.. [7] https://doc.qt.io/
.. [8] ``qttools/util/recolordocsicons/README.md``
.. [9] https://doc.qt.io/qtcreator/creator-how-to-record-screens.html
.. [10] https://www.screentogif.com/
